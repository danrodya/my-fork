def bold(s: str):
    new_text = ''
    first_mark = True
    i = 0
    while i < len(s):
        if s[i] == '*' and i != len(s) - 1:
            if s[i + 1] == '*' and first_mark:
                new_text = new_text + '<strong>'
                i = i + 1
                first_mark = False
            elif s[i + 1] == '*' and not first_mark:
                new_text = new_text + '</strong>'
                i = i + 1
                first_mark = True
            else:
                new_text = new_text + s[i]
        else:
            new_text = new_text + s[i]
        i = i + 1

    return new_text


def italic(s: str):
    new_text = ''
    first_mark = True
    i = 0
    while i < len(s):
        if s[i] == '*' and i != len(s) - 1:
            if s[i + 1] != '*' and first_mark:
                new_text = new_text + '<em>'
                first_mark = False
            elif s[i + 1] != '*' and not first_mark:
                new_text = new_text + '</em>'
                first_mark = True
            else:
                new_text = new_text + s[i]
        elif s[i] == '*' and i == len(s) - 1:
            new_text = new_text + '</em>'
        else:
            new_text = new_text + s[i]
        i = i + 1

    return new_text


if __name__ == '__main__':
    text = "*text* **bold** *italic*"
    #new = text.split(" ")
    #result = new[0].replace("**","<strong>")
    text = bold(text)
    print(text)
    text = italic(text)
    print(text)